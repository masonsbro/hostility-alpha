Client to Server Packet Format

unsigned int protocolVersion
	Current Version = 0
unsigned byte packetType
	0 = Keep Alive
	1 = Login
	2 = Chat
	3 = Special Action (pick up item, flip lever, etc)
	4 = Movement Start (forward, backward, left, right)
	5 = Movement Stop (forward, backward, left, right)
	6 = Rotation Change
	7 = Start Shooting
	8 = Stop Shooting
	9 = Change Weapon
	10 = Start Aiming
	11 = Stop Aiming
	12 = Melee

Login:
	char[32] displayName (null padded)
	char[15] IP
	unsigned short port
Chat:
	char[512] message (null padded)
Special Action:
Movement Start:
	byte dir
		0 = Forward
		1 = Backward
		2 = Left
		3 = Right
Movement Stop:
	byte dir
		0 = Forward
		1 = Backward
		2 = Left
		3 = Right
Rotation Change: # Only sent once per server tick, not every time there's a rotation change
	short heading
	short pitch
	short roll
Start Shooting:
Stop Shooting:
Change Weapon:
Start Aiming: # Increases accuracy
Stop Aiming:
Melee:

Server to Client Packet Format

unsigned int protocolVersion
	Current Version = 0
unsigned int packetType
	0 = Kick
	1 = Create Entity
	2 = Create World Object
	3 = End World Init
	4 = Entity Update
	5 = Player List
	6 = Chat
	7 = Sound

Kick:
	char[256] message
Create Entity:
	unsigned int entityID
	unsigned int entityType
Create World Object:
	unsigned int worldObjectType
	unsigned int worldObjectX
	unsigned int worldObjectY
	unsigned int worldObjectZ
	short worldObjectH
	short worldObjectP
	short worldObjectR
Entity Update:
	unsigned int entityID
	unsigned int entityX
	unsigned int entityY
	unsigned int entityZ
	short entityH
	short entityP
	short entityR
Player List:
	byte numPlayers
	char[32] playerName (repeat numPlayers times)
Chat:
	char[32] playerName
	char[512] message
Sound:
	char[32] soundName
	unsigned int posX
	unsigned int posY
	unsigned int posZ