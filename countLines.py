import os

serverLines = 0
serverLinesExcludingBlanks = 0
for dirname, dirnames, filenames in os.walk('serverFiles'):
    for filename in filenames:
    	if filename.endswith(".py"):
	    	f = open(os.path.join(dirname, filename), "r")
	    	lines = f.readlines()
	    	numLines = len(lines)
	    	numLinesExcludingBlanks = len([line for line in lines if line.strip() != ""])
	    	serverLines += numLines
	    	serverLinesExcludingBlanks += numLinesExcludingBlanks
	    	f.close()

clientLines = 0
clientLinesExcludingBlanks = 0
for dirname, dirnames, filenames in os.walk('clientFiles'):
    for filename in filenames:
    	if filename.endswith(".py"):
	    	f = open(os.path.join(dirname, filename), "r")
	    	lines = f.readlines()
	    	numLines = len(lines)
	    	numLinesExcludingBlanks = len([line for line in lines if line.strip() != ""])
	    	clientLines += numLines
	    	clientLinesExcludingBlanks += numLinesExcludingBlanks
	    	f.close()

print "Number of lines in server code: " + str(serverLines)
print "Number of lines in client code: " + str(clientLines)
print "Number of lines total:          " + str(serverLines + clientLines)
print
print "Number of lines in server code excluding blank lines: " + str(serverLinesExcludingBlanks)
print "Number of lines in client code excluding blank lines: " + str(clientLinesExcludingBlanks)
print "Number of lines total excluding blank lines:          " + str(serverLinesExcludingBlanks + clientLinesExcludingBlanks)
