from server import Server

# ServerCampaign contains SaveGame and Client information

class ServerCampaign(Server):
	def __init__(self):
		Server.__init__(self)
		self.saveGame = None
	def setSaveGame(self, saveGame):
		self.saveGame = saveGame
	# Must override the standard getWorld()
	def getWorld(self):
		return self.saveGame.getWorld()
	def customServerTick(self):
		print "Server Tick Report"
		print
		print "Entities: " + str(self.getWorld().entities)
		print "Players: " + str(self.getWorld().players)
		print
		print