# World contains all the entities, world objects, players (also in entities list), etc. in active world. It should be notified every game tick to update all entities. World does NOT need to know what type of world this is (free roam, multiplayer, single player, mission, etc), and no subclasses for these types are necessary.

class World:
	def __init__(self):
		self.worldObjects = []
		self.entities = []
		self.players = []
		self.spawnX = 0
		self.spawnY = 0
		self.spawnZ = 0
		self.nextEntityID = 0
	def getPlayers(self):
		return self.players
	def getEntities(self):
		return self.entities
	def getWorldObjects(self):
		return self.worldObjects
	def addEntities(self, *entities):
		self.entities.extend(list(entities))
	def addWorldObjects(self, *objects):
		self.worldObjects.extend(list(objects))
	def addPlayer(self, player):
		self.players.append(player)
		self.addEntities(player)
	def removeEntities(self, *entities):
		for entity in entities:
			self.entities.remove(entity)
	def removeWorldObjects(self, *objects):
		for object in objects:
			self.worldObjects.remove(object)
	def removePlayer(self, player):
		self.players.remove(player)
		self.entities.remove(player)
	def getEntityUpdates(self):
		updates = []
		for entity in self.entities:
			updates.extend(entity.getUpdates())
		return updates
	def getEntityAIUpdates(self):
		return []
	def getSpawnX(self):
		return self.spawnX
	def getSpawnY(self):
		return self.spawnY
	def getSpawnZ(self):
		return self.spawnZ
	def setSpawnX(self, spawnX):
		self.spawnX = spawnX
	def setSpawnY(self, spawnY):
		self.spawnY = spawnY
	def setSpawnZ(self, spawnZ):
		self.spawnZ = spawnZ
	def getNextEntityID(self):
		return self.nextEntityID
	def incNextEntityID(self):
		self.nextEntityID += 1
	# Return True if the specified coordinates have a lower Y value than the heightmap at the same X and Z coordinates
	def areCoordsUnderHeightmap(self, x, y, z):
		return False
	def getAndIncNextEntityID(self):
		self.incNextEntityID()
		return self.getNextEntityID() - 1