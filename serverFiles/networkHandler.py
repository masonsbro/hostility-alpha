import socket
import struct

# NetworkHandler has a list of all clients to whom to send updates.

class NetworkHandler:
	PROTOCOL_VERSION = 0
	def __init__(self):
		self.clients = []
		self.packetsPending = []
	def addClient(self, client):
		self.clients.append(client)
	def removeClient(self, client):
		self.clients.remove(client)
	def getClients(self):
		return self.clients
	def sendPacket(self, packet, client):
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		s.sendto(packet, client)
		s.close()
	def addEvent(self, event):
		# If event is serializable, serialize it and add it to packet list
		# Otherwise, discard -- network handler can't do anything about it
		serialization = event.serialize()
		if serialization:
			self.packetsPending.append(struct.pack("!I", self.PROTOCOL_VERSION) + serialization)
	def updateClients(self):
		# Send each packet to each client
		# Loops nested in this way so that each client gets respective packets at about the same time
		# Don't want one client to receive 200 packets and then 1 second later another client receives the same ones
		for packet in self.packetsPending:
			for client in self.clients:
				self.sendPacket(packet, client)
		# Empty pending packets list when done
		self.packetsPending = []
	def initClient(self, client, world):
		# When initializing a client (giving client the whole world upon connect), need a special method.
		protocolVersion = 0
		packetType = 0
		numEntities = len(world.getEntities())
		entitiesSection = ""
		for entity in world.getEntities():
			entitiesSection.append(struct.pack("IIIIIhhh", entity.entityID, entity.getType(), ))