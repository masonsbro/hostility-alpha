import missions.mission001

# SaveGame contains an instance of every mission. These instances never change, the same instances of the missions will be used every time a save game is loaded. Each mission also has its own world object which will only be set to None once the mission is completed. To restart the mission, the misison must be deleted and reinitialized.

class SaveGame:
	def __init__(self):
		self.activeMission = None
		self.missions = []
		# Later, automatically detect all installed missions
		self.missions.append(missions.mission001.Mission001())
	def setActiveMissionByNumber(self, number):
		self.activeMission = self.missions[number - 1]
	def setActiveMission(self, mission):
		self.activeMission = mission
	def getActiveMission(self):
		return self.activeMission
	def getActiveMissionNumber(self):
		return self.missions.index(self.activeMission)
	def getWorld(self):
		return self.activeMission.getWorld()