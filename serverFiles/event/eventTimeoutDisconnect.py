# EventTimeoutDisconnect represents a player disconnecting because of a timeout (client didn't send packet for 10 seconds)

from event import Event

class EventTimeoutDisconnect(Event):
	def __init__(self, client):
		Event.__init__(self)
		self.client = client
	def onServerReceive(self, server):
		server.removeClientAndPlayer(self.client)