from event import Event
from object import entityPlayer

# EventKick to kick a player (with a message)

class EventKick(Event):
	def __init__(self, playerName, playerAddress, message = "You have been kicked."):
		Event.__init__(self)
		self.playerName = playerName
		self.playerAddress = playerAddress
		self.message = message
	def onServerReceive(self, server):
		server.removeClientAndPlayer(self.playerAddress)
		return [EventSendPacket(struct.pack("!IB128s", Server.PROTOCOL_VERSION, 0, self.message)]