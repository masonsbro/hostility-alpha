from event import Event
from object import entityPlayer
import struct

# EventLogin for when a player logs in

class EventLogin(Event):
	packet_id = 1
	def __init__(self, playerName, playerAddress):
		Event.__init__(self)
		self.playerName = playerName
		self.playerAddress = playerAddress
	@classmethod
	def deserialize(eventClass, packet):
		# char[32] displayName
		# char[15] IP
		# unsigned short port

		(displayName, host, port) = struct.unpack("!32s15sH", packet[0][5:])

		return eventClass(displayName, (host, port))
	def serialize(self):
		return struct.pack("!B32s15sH", self.packet_id, self.playerName, self.playerAddress[0], self.playerAddress[1])
	def onServerReceive(self, server):
		# TODO: Redesign this method. It's kind of ghetto and it relies on try/except... use if/else.
		try:
			server.addPlayer(entityPlayer.EntityPlayer(server.getNextEntityID(), server.getSpawnX(), server.getSpawnY(), server.getSpawnZ(), 0, 0, 0), self.playerAddress)
		except:
			# TODO: Send message to client?
			pass