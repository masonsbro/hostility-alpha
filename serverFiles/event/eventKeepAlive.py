from event import Event
import struct

class EventKeepAlive(Event):
	packet_id = 0
	def __init__(self, client):
		Event.__init__(self)
		self.client = client
	@classmethod
	def deserialize(eventClass, packet):
		# Woo! No data!
		return eventClass(packet[1])
	def serialize(self):
		return struct.pack("!B", self.packet_id)
	def onServerReceive(self, server):
		server.updateClientTimeout(self.client)