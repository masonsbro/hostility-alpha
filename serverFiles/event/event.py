# Base Event class should be subclassed.

class Event:
	listeners = []
	def __init__(self):
		self.canceled = False
	@classmethod
	def addListener(eventClass, listener):
		eventClass.listeners.append(listener)
	# Maybe should be static method instead -- keeping as class method for subtyping real events
	@classmethod
	def deserialize(eventClass, packet):
		return eventClass()
	def serialize(self):
		# Note: Only returns data PAYLOAD, not entire packet (packet is two parts), and not even the whole data part, just the payload (after the protocol version)
		return ""
	# Don't touch these two methods; they are wrappers for the real ones
	def serverExecute(self, server):
		for listener in self.__class__.listeners:
			listener(self)
		if not self.isCanceled():
			result = self.onServerReceive(server)
			if result != None:
				return result
			return []
	def clientExecute(self, client):
		for listener in self.__class__.listeners:
			listener(self)
		if not self.isCanceled():
			result = self.onClientReceive(client)
			if result != None:
				return result
			return []
	# Override these two methods in subclasses, cancel-checking built-in
	def onServerReceive(self, server):
		# When a server receives the event from a client
		pass
	def onClientReceive(self, client):
		# When a client receives the event from a server
		pass
	def setCanceled(self, canceled):
		self.canceled = canceled
	def isCanceled(self):
		return self.canceled