from event import Event

# EventEntityPosChange for when an entity's position changes

class EventEntityPosChange(Event):
	def __init__(self, entity, x, y, z):
		Event.__init__(self)
		self.entity = entity
		self.x = x
		self.y = y
		self.z = z
	def onServerReceive(self, server):
		self.entity.setPos(self.x, self.y, self.z)
		if server.getWorld().areCoordsUnderHeightmap(self.x, self.y, self.z):
			return []