from event import Event

# EventEntitySpeedChange for when an entity's speed changes

class EventEntitySpeedChange(Event):
	def __init__(self, entity, newSpeedX, newSpeedY, newSpeedZ):
		Event.__init__(self)
		self.entity = entity
		self.speedX = newSpeedX
		self.speedY = newSpeedY
		self.speedZ = newSpeedZ
	def onServerReceive(self, server):
		self.entity.setSpeed(self.speedX, self.speedY, self.speedZ)