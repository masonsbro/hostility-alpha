import packetHandler
import networkHandler
import serverEventParser
from event import eventTimeoutDisconnect
from event import eventKeepAlive
from event import eventLogin

import time
import os
import traceback

class Server:
	PROTOCOL_VERSION = 0
	def __init__(self):
		# Create and start packet handler
		self.packetHandler = packetHandler.PacketHandler()
		# Create network handler to send updates to clients
		self.networkHandler = networkHandler.NetworkHandler()
		# Create event parser
		self.eventParser = serverEventParser.ServerEventParser()
		# Add default events
		self.addDefaultPacketParserEvents()
		# Add custom events
		self.addCustomPacketParserEvents()
		# Add custom event listeners
		self.addCustomListeners()
		# Maintain dict of clients and how much time they have left before timing out
		self.clientToTicksLeft = {}
		# Maintain dict of clients and what player entity they represent
		self.clientToEntityPlayer = {}
	def addDefaultPacketParserEvents(self):
		self.eventParser.addPacketType(0, eventKeepAlive.EventKeepAlive)
		self.eventParser.addPacketType(1, eventLogin.EventLogin)
	def addCustomPacketParserEvents(self):
		pass
	def addCustomListeners(self):
		pass
	def updateGameState(self):
		# Get packet queue from network
		packets = self.packetHandler.getPackets()
		# Turn into event queue
		events = self.eventParser.parseEvents()
	def getNetworkEvents(self):
		# Get packet queue from network
		packets = self.packetHandler.getPackets()
		# Turn into event queue
		return self.eventParser.parsePackets(packets)
	# Must be overridden by custom servers
	def getWorld(self):
		raise Exception("This function should be overridden by a subclass of Server")
	def addPlayer(self, player, playerAddress):
		playerAddress = (playerAddress[0].strip("\0 \t\n"), playerAddress[1])
		if playerAddress in self.networkHandler.getClients():
			raise Exception("Player already exists")
		self.getWorld().addPlayer(player)
		self.networkHandler.addClient(playerAddress)
		self.clientToTicksLeft[playerAddress] = 10 * 1.0
		self.clientToEntityPlayer[playerAddress] = player
	def getNextEntityID(self):
		return self.getWorld().getAndIncNextEntityID()
	# Runs in main thread
	def startServer(self):
		self.packetHandler.start()
		while True:
			try:
				self.doTick()
			# Exit all threads (even listener) on exception
			except:
				traceback.print_exc()
				os._exit(1)
	def doTick(self):
		# Game loop:
		# 	1. Get packets
		# 	2. Get events (and physics, entity, and AI events)
		# 	3. Handle events
		# 	4. Send updated world to all clients
		# Before all the events have been handled, do custom server tick stuff
		start = time.time()
		self.customServerTick()
		events = self.getNetworkEvents()
		events.extend(self.getWorld().getEntityUpdates())
		events.extend(self.getWorld().getEntityAIUpdates())
		events.extend(self.getTimeouts())
		events.extend(self.customServerEvents())
		# Let events create new events
		while events:
			newEvents = []
			for event in events:
				# It's okay to give the network handler even the events that can't be serialized
				# It will discard them
				self.networkHandler.addEvent(event)
				newEvents.extend(event.serverExecute(self))
			events = newEvents[:]
		# Send updated game world to all clients
		self.networkHandler.updateClients()
		for client in self.clientToTicksLeft.keys():
			self.clientToTicksLeft[client] -= 1
		time.sleep(((1000.0 / 1.0) - ((time.time() - start) * 1000.0)) / 1000.0)
	# Should be called to get a list of disconnect events because of timeouts
	def getTimeouts(self):
		events = []
		for client, timeout in self.clientToTicksLeft.iteritems():
			if timeout <= 0:
				events.append(eventTimeoutDisconnect.EventTimeoutDisconnect(client))
		return events
	# Removes a player from the world and a client from all server and network manager representations
	def removeClientAndPlayer(self, client):
		self.getWorld().removePlayer(self.clientToEntityPlayer[client])
		self.networkHandler.removeClient(client)
		del self.clientToTicksLeft[client]
		del self.clientToEntityPlayer[client]
	# Should be called to reset a client's timeout timer because a packet from that client was received
	def updateClientTimeout(self, client):
		self.clientToTicksLeft[client] = 10 * 1.0
	# Should be overridden when the custom server needs to do something every tick, like check whether the game has been won
	def customServerTick(self):
		pass
	# Should be overridden when the custom server needs to create its own events and return a list of them
	def customServerEvents(self):
		return []
	def getSpawnX(self):
		return self.getWorld().getSpawnX()
	def getSpawnY(self):
		return self.getWorld().getSpawnY()
	def getSpawnZ(self):
		return self.getWorld().getSpawnZ()