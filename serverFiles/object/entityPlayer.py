import entityAnimated
from event import eventEntitySpeedChange

# EntityPlayer stores player information and its position and rotation are usually used for the camera

class EntityPlayer(entityAnimated.EntityAnimated):
	def jump(self):
		return eventEntitySpeedChange.EventEntitySpeedChange(self, self.speedX, 1, self.speedZ)