import gameObject
from event import eventEntitySpeedChange, eventEntityPosChange

# Entities include Game Objects whose position and/or rotation can change, and must be saved to disk and read from disk. These cannot be accurately recreated upon reloading of the mission without being saved to disk. Entities also are updated every game tick and can change position.

class Entity(gameObject.GameObject):
	def __init__(self, entityID, x = 0, y = 0, z = 0, h = 0, p = 0, r = 0, scaleX = 1, scaleY = 1, scaleZ = 1):
		gameObject.GameObject.__init__(self, x, y, z, h, p, r, scaleX, scaleY, scaleZ)
		self.speedX = 0
		self.speedY = 0
		self.speedZ = 0
		self.entityID = entityID
		self.onGround = True
	def __str__(self):
		return "Entity X = " + str(self.posX) + "; Y = " + str(self.posY) + "; Z = " + str(self.posZ) + "; ID = " + str(self.entityID) + "\n"
	def __repr__(self):
		return self.__str__()
	def setSpeed(self, speedX, speedY, speedZ):
		self.speedX = speedX
		self.speedY = speedY
		self.speedZ = speedZ
	def addSpeed(self, speedX, speedY, speedZ):
		self.speedX += speedX
		self.speedY += speedY
		self.speedZ += speedZ
	def getSpeed(self):
		return self.speedX, self.speedY, self.speedZ
	def getUpdates(self):
		events = []
		events.extend(self.getGravityUpdates())
		events.extend(self.getMoveUpdates())
		return events
	# Apply gravity
	def getGravityUpdates(self):
		events = []
		# Apply negative acceleration due to gravity and cap at -1 unit per tick (terminal velocity)
		if self.speedY > -1 and not self.onGround:
			newSpeedY = self.speedY - 0.1
			if newSpeedY < -1:
				newSpeedY = -1
			events.append(eventEntitySpeedChange.EventEntitySpeedChange(self, self.speedX, newSpeedY, self.speedZ))
		return events
	def getMoveUpdates(self):
		return [eventEntityPosChange.EventEntityPosChange(self, self.posX + self.speedX, self.posY + self.speedY, self.posZ + self.speedZ)]