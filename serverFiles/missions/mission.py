import world

# Mission is the base mission class all missions should inherit. Missions are updated every game tick. This updating includes a goal check, which returns True if the mission should continue and False if it is over. This value is also returned from the update function.

# A mission class contains:
#	Location of world objects
#	Starting location of entities
#	Starting player location
#	Custom mission GUI handler
#	Goal checks

# A mission instance contains:
#	A world (which contains all the game objects)

class Mission:
	def __init__(self):
		self.setMissionName()
		self.world = world.World()
		self.initWorldObjects()
		self.initEntities()
	def update(self):
		if not self.world:
			return False
		return self.checkForGoal()
	def initWorldObjects(self):
		pass
	def initEntities(self):
		pass
	def addPlayer(self, player):
		self.world.addPlayer(player)
	# Return False to end mission, True to continue. All missions should override this.
	def checkForGoal(self):
		return False
	def getWorld(self):
		return self.world
	def cleanup(self):
		self.world = None
	def setMissionName(self):
		self.missionName = ""