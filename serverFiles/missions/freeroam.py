import scene.freeroamScene

# This does not inherit from Mission, but has a similar structure

class FreeRoam:
	def __init__(self):
		self.world = world.World()
		self.initWorldObjects()
		self.initEntities()
	def initWorldObjects(self):
		pass
	def initEntities(self):
		pass
	def addPlayer(self, player):
		self.world.addPlayer(player)
	def getWorld(self):
		return self.world
	def cleanup(self):
		self.world = None