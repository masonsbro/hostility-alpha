import struct

# ServerEventParser takes packets and parses events from them: it should be able to read packets and create specific events from them

class ServerEventParser:
	PROTOCOL_VERSION = 0
	def __init__(self):
		self.packetTypes = {}
	def addPacketType(self, type, cls):
		self.packetTypes[type] = cls
	def getPacketType(self, packet):
		return struct.unpack("!B", packet[0][4:5])[0]
	def parsePacket(self, packet):
		packetType = self.getPacketType(packet)
		# Use dictionary for switch/case
		# Detect bad packets and handle gracefully
		try:
			return self.packetTypes[packetType].deserialize(packet)
		except:
			print "Possible attack (malformed packet): " + str(packet[1])
			return None
	def parsePackets(self, packets):
		events = []
		for packet in packets:
			event = self.parsePacket(packet)
			if event != None:
				events.append(event)
		return events