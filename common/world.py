# World contains all the entities, world objects, players (also in entities list), etc. in active world. It should be notified every game tick to update all entities. World does NOT need to know what type of world this is (free roam, multiplayer, single player, mission, etc), and no subclasses for these types are necessary.

class World:
	def __init__(self):
		self.worldObjects = []
		self.entities = []
		self.players = []
	def updateEntities(self):
		for entity in self.entities:
			entity.update()
			entity.updateAI()
	def getPlayers(self):
		return self.players
	# Revisit: Is this method necessary?
	def getFirstPlayer(self):
		if len(self.players) > 0:
			return self.players[0]
		return None
	def getEntities(self):
		return self.entities
	def getWorldObjects(self):
		return self.worldObjects
	def addEntities(self, *entities):
		self.entities += list(entities)
	def addWorldObjects(self, *objects):
		self.worldObjects += list(objects)
	def addPlayer(self, player):
		self.players.append(player)
	def removeEntities(self, *entities):
		for entity in entities:
			self.entities.remove(entity)
	def removeWorldObjects(self, *objects):
		for object in objects:
			self.worldObjects.remove(object)
	def removePlayer(self, player):
		self.players.remove(player)