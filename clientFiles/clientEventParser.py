from event.eventDisconnect import EventDisconnect

class ClientEventParser:
	def __init__(self, protocol):
		self.packetTypes = {}
		self.protocol = protocol
	def addPacketType(self, type, callback):
		self.packetTypes[type] = callback
	def getPacketProtocol(self, packet):
		return struct.unpack("!I", packet[0][0:4])[0]
	def getPacketType(self, packet):
		return struct.unpack("!B", packet[0][4:5])[0]
	def parsePacket(self, packet):
		packetType = self.getPacketType(packet)
		# If packet is wrong protocol version, add quit event
		if self.getPacketProtocol(packet) != self.protocol:
			return [EventDisconnect()]
		# Use dictionary for switch/case
		# Detect bad packets and handle gracefully
		try:
			return self.packetTypes[packetType](packet)
		except:
			print "Malformed packet: " + str(packet[1])
			return None
	def parsePackets(self, packets):
		events = []
		for packet in packets:
			event = self.parsePacket(packet)
			if event is not None:
				events.extend(event)
		return events