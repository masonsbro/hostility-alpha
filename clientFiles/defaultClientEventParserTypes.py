from event.eventUpdateEntity import EventUpdateEntity

class DefaultClientEventParserTypes:

	def parseEntityUpdate(self, packet):
		# unsigned int entityID
		# unsigned int entityX
		# unsigned int entityY
		# unsigned int entityZ
		(entityID, entityX, entityY, entityZ, entityH, entityP, entityR) = struct.unpack("!IIIIhhh", packet[0][5:])
		return EventUpdateEntity(entityID, entityX, entityY, entityZ, entityH, entityP, entityR)