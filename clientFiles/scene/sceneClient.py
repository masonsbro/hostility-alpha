# SceneClient holds all client-specific game state data, including GUI, NodePaths, and key states.

class SceneClient:
	def __init__(self):
		self.nodes = []
		self.setUpKeys()
		self.world = None
	def destroyScene(self):
		for node in self.nodes:
			node.removeNode()
			self.nodes.remove(node)
	def addNode(self, node):
		self.nodes.append(node)
	def setUpKeys(self):
		self.keys = {}
	def update(self):
		pass
	def keyPress(self, key):
		if key[-3:] == "-up":
			self.keys[key[0:-3]] = False
		else:
			self.keys[key] = True
	def isKeyPressed(self, key):
		if key in self.keys:
			return self.keys[key]
		else:
			self.keys[key] = False
			return False