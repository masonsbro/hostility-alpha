# SceneMission is the basic mission scene (world and GUI); it can be overridden per mission.

import scene

class SceneMission(scene.Scene):
	def __init__(self, mission = None):
		scene.Scene.__init__(self)
		self.createHUD()
		self.mission = mission
	def createHUD(self):
		pass
	def update(self):
		isMoving = False
		if self.keyIsPressed("w"):
			isMoving = True
		if self.keyIsPressed("a"):
			isMoving = True
		if self.keyIsPressed("s"):
			isMoving = True
		if self.keyIsPressed("d"):
			isMoving = True
		if isMoving:
			pass