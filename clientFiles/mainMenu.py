import scene.scene
import mainMenuActionSet

#from direct.gui.DirectGui import DirectButton

class SceneMainMenu(scene.scene.Scene):
	def __init__(self, app):
		scene.scene.Scene.__init__(self)
		self.actions = mainMenuActionSet.MainMenuActions(app)
		self.createButtons()
	def createButtons(self):
		startGameButtonModel = loader.loadModel("media/startgame.egg")
		startGameButton = DirectButton(
			geom = (startGameButtonModel, startGameButtonModel, startGameButtonModel, startGameButtonModel),
			relief = None,
			command = self.actions.newGame)
		startGameButton.setScale(.5, 1, .2)
		startGameButton.reparentTo(aspect2d)
		self.addNode(startGameButton)