from client import Client

class Game:
	def __init__(self, username):
		self.username = username
	def createClient(self, serverAddress):
		self.client = Client(self, serverAddress, self.username)
	def endClient(self):
		# Clean up client
		return