import socket
import threading

# PacketHandlerClient is a subclass of Thread and should be run as such. When it accepts a connection, it adds it to an internal buffer for handling incoming connections. Also has a flush function to return and empty the buffer.

class PacketHandlerClient(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)
		self.packets = []
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.sock.bind(("", 9586))
	def run(self):
		while True:
			# Read one packet
			data, address = self.sock.recvfrom(65535)
			# Add packet to packets queue in form (data, (host, port))
			self.packets.append((data, address))
	def getPackets(self):
		returnPackets = self.packets
		self.packets = []
		return returnPackets