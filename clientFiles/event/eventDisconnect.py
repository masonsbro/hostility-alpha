from .event import Event

class EventDisconnect(Event):
	def __init__(self):
		Event.__init__(self)
	def onClientReceive(self, client):
		# Disconnect client
		client.game.endClient()