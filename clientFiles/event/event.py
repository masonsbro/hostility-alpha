import struct

# Base Event class should be subclassed.

class Event:
	listeners = []
	packetType = 0
	def __init__(self):
		self.canceled = False
	@classmethod
	def addListener(eventClass, listener):
		eventClass.listeners.append(listener)
	# Don't touch these two methods; they are wrappers for the real ones
	def serverExecute(self, server):
		for listener in self.__class__.listeners:
			listener(self)
		if not self.isCanceled():
			result = self.onServerReceive(server)
			if result != None:
				return result
			return []
	def clientExecute(self, client):
		for listener in self.__class__.listeners:
			listener(self)
		if not self.isCanceled():
			result = self.onClientReceive(client)
			if result != None:
				return result
			return []
	# These two methods send a packet representing this event using the client's or server's network handler
	def clientSerialize(self, client):
		#client.networkHandler.sendPacket(struct.pack("!IB", client.PROTOCOL_VERSION, self.packetType))
		pass
	def serverSerialize(self, server):
		#server.networkHandler.sendPacket(struct.pack("!IB", server.PROTOCOL_VERSION, self.packetType))
		pass
	# Override these two methods in subclasses, cancel-checking built-in
	def onServerReceive(self, server):
		# When a server receives the event from a client
		pass
	def onClientReceive(self, client):
		# When a client receives the event from a server
		pass
	def setCanceled(self, canceled):
		self.canceled = canceled
	def isCanceled(self):
		return self.canceled