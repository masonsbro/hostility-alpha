from event import Event

class EventUpdateEntity(Event):
	def __init__(self, entityID, entityX, entityY, entityZ, entityH, entityP, entityR):
		Event.__init__(self)
		self.entityID = entityID
		self.entityX = entityX
		self.entityY = entityY
		self.entityZ = entityZ
		self.entityH = entityH
		self.entityP = entityP
		self.entityR = entityR
	def onClientReceive(self, client):
		e = client.getEntity(self.entityID)
		e.setPos(self.entityX, self.entityY, self.entityZ)
		e.setHPR(self.entityH, self.entityP, self.entityR)