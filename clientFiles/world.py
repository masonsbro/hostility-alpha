# World contains all the entities, world objects, players (also in entities list), etc. in active world. It should be notified every game tick to update all entities. World does NOT need to know what type of world this is (free roam, multiplayer, single player, mission, etc), and no subclasses for these types are necessary.

class World:
	def __init__(self):
		self.worldObjects = []
		self.entities = []
		self.players = []
		self.spawnX = 0
		self.spawnY = 0
		self.spawnZ = 0
		self.nextEntityID = 0
	def getPlayers(self):
		return self.players
	def getEntities(self):
		return self.entities
	def getWorldObjects(self):
		return self.worldObjects
	def addEntities(self, *entities):
		self.entities.extend(list(entities))
	def addWorldObjects(self, *objects):
		self.worldObjects.extend(list(objects))
	def addPlayer(self, player):
		self.players.append(player)
		self.entities.append(player)
	def removeEntities(self, *entities):
		for entity in entities:
			self.entities.remove(entity)
	def removeWorldObjects(self, *objects):
		for object in objects:
			self.worldObjects.remove(object)
	def removePlayer(self, player):
		self.players.remove(player)
		self.entities.remove(player)
	def getEntity(self, entityID):
		for entity in self.entities:
			if entity.entityID == entityID:
				return entity