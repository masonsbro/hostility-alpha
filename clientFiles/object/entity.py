import gameObject

# Entities include Game Objects whose position and/or rotation can change, and must be saved to disk and read from disk. These cannot be accurately recreated upon reloading of the mission without being saved to disk. Entities also are updated every game tick and can change position.

class Entity(gameObject.GameObject):
	def __init__(self, type, entityID, x = 0, y = 0, z = 0, h = 0, p = 0, r = 0, scaleX = 1, scaleY = 1, scaleZ = 1):
		gameObject.GameObject.__init__(self, x, y, z, h, p, r, scaleX, scaleY, scaleZ)
		self.speedX = 0
		self.speedY = 0
		self.speedZ = 0
		self.entityID = entityID
		self.onGround = True
	def __str__(self):
		return "Entity X = " + str(self.posX) + "; Y = " + str(self.posY) + "; Z = " + str(self.posZ) + "; ID = " + str(self.entityID) + "\n"
	def __repr__(self):
		return self.__str__()
	def setSpeed(self, speedX, speedY, speedZ):
		self.speedX = speedX
		self.speedY = speedY
		self.speedZ = speedZ
	def addSpeed(self, speedX, speedY, speedZ):
		self.speedX += speedX
		self.speedY += speedY
		self.speedZ += speedZ
	def getSpeed(self):
		return self.speedX, self.speedY, self.speedZ