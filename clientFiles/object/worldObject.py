import gameObject

# World Objects include only static objects that should never change in position/rotation after initialization. Persistance is not necessary for World Objects.

class WorldObject(gameObject.GameObject):
	pass