# Game Objects include all objects in the game, static or dynamic.

class GameObject:
	def __init__(self, x = 0, y = 0, z = 0, h = 0, p = 0, r = 0, scaleX = 1, scaleY = 1, scaleZ = 1):
		self.posX = x
		self.posY = y
		self.posZ = z
		self.rotH = h
		self.rotP = p
		self.rotR = r
		self.scaleX = scaleX
		self.scaleY = scaleY
		self.scaleZ = scaleZ
	def setPos(self, x, y, z):
		self.posX = x
		self.posY = y
		self.posZ = z
	def addPos(self, x, y, z):
		self.posX += x
		self.posY += y
		self.posZ += z
	def getPos(self):
		return (self.posX, self.posY, self.posZ)
	def setHPR(self, h, p, r):
		self.rotH = h
		self.rotP = p
		self.rotR = r
	def addHPR(self, h, p, r):
		self.rotH += h
		self.rotP += p
		self.rotR += r
	def getHPR(self):
		return (self.rotH, self.rotP, self.rotR)
	def setScale(self, x, y, z):
		self.scaleX = x
		self.scaleY = y
		self.scaleZ = z
	def addScale(self, x, y, z):
		self.scaleX += x
		self.scaleY += y
		self.scaleZ += z
	def getScale(self):
		return (self.scaleX, self.scaleY, self.scaleZ)