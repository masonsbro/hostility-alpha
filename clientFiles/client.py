import packetHandlerClient
import networkHandlerClient
import clientEventParser
import defaultClientEventParserTypes
from world import World

import time, struct

class Client:
	PROTOCOL_VERSION = 0
	def __init__(self, game, serverAddress, username):
		# Remember username
		self.username = username
		# Keep track of parent game
		self.game = game
		# Create and start packet handler
		self.packetHandler = packetHandlerClient.PacketHandlerClient()
		# Create network handler to send updates to server
		self.networkHandler = networkHandlerClient.NetworkHandlerClient(serverAddress)
		# Create event parser
		self.eventParser = clientEventParser.ClientEventParser(Client.PROTOCOL_VERSION)
		# Add default events
		self.addDefaultPacketParserEvents()
		# Add custom events
		self.addCustomPacketParserEvents()
		# Add custom event listeners
		self.addCustomListeners()
		# Create world
		self.world = World()
		# Keep server address and connect
		self.serverAddress = serverAddress
		self.networkHandler.sendPacket(struct.pack("!IB32s15sH", Client.PROTOCOL_VERSION, 1, username, "localhost", 9586))
	def addDefaultPacketParserEvents(self):
		defaultEventClass = defaultClientEventParserTypes.DefaultClientEventParserTypes()
		#self.eventParser.addPacketType(packetType, func)
	def addCustomPacketParserEvents(self):
		pass
	def addCustomListeners(self):
		pass
	def updateGameState(self):
		return []
	def getNetworkEvents(self):
		# Get packet queue from network
		packets = self.packetHandler.getPackets()
		# Turn into event queue
		return self.eventParser.parsePackets(packets)
	# Must be overridden by custom servers
	def getWorld(self):
		return self.world
	def addPlayer(self, player, playerAddress):
		self.getWorld().addPlayer(player)
		playerAddress = (playerAddress[0].strip("\0 \t\n"), playerAddress[1])
		self.networkHandler.addClient(playerAddress)
		self.clientToTicksLeft[playerAddress] = 10 * 1.0
		self.clientToEntityPlayer[playerAddress] = player
	# Runs in main thread
	def startClient(self):
		self.packetHandler.start()
		while True:
			self.doTick()
	def doTick(self):
		# Game loop:
		# 	1. Get packets
		# 	2. Get events
		# 	3. Handle events
		# Before all the events have been handled, do custom client tick stuff
		start = time.time()
		self.customClientTick()
		events = self.getNetworkEvents()
		events.extend(self.getTimeouts())
		events.extend(self.customClientEvents())
		events.extend(self.updateGameState())
		# Let events create new events
		while events:
			newEvents = []
			for event in events:
				newEvents.extend(event.clientExecute(self))
			events = newEvents[:]
		time.sleep(((1000.0 / 1.0) - ((time.time() - start) * 1000.0)) / 1000.0)
	# Removes a player from the world
	def removeClientAndPlayer(self, client):
		self.getWorld().removePlayer(self.clientToEntityPlayer[client])
	# Should be overridden when the custom client needs to do something every tick
	def customClientTick(self):
		pass
	# Should be overridden when the custom client needs to create its own events and return a list of them
	def customClientEvents(self):
		return []
	def getEntity(self, entityID):
		return self.getWorld().getEntity(entityID)