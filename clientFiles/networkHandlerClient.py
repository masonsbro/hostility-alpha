import socket

# NetworkHandlerClient sends updates to the server about the client's activity

class NetworkHandlerClient:
	def __init__(self, serverAddr):
		self.serverAddr = serverAddr
		self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.s.bind(("", 53000))
	def setServerAddr(self, serverAddr):
		self.serverAddr = serverAddr
	def sendPacket(self, packet):
		self.s.sendto(packet, self.serverAddr)
	def closeConnection(self):
		self.s.close()